import React from 'react';
import './App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCopyright } from '@fortawesome/free-regular-svg-icons';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Homepage from './Components/Homepage/Homepage';
import CocktailList from './Components/CocktailList/CocktailList';
import logo from './Asset/images/logo-white.png';


function App() {
  return (
    <div className="App">
      <Router>
        <header className="container-fuild text-center fixed-top">
        <div className="row justify-content-center">
          <div className="col-12 pt-5 pb-3">
            <img src={logo} alt="logo" className="logo"/>
          </div>
          <div className="col-1 py-2 linkHeader">
            <Link to="/">Home</Link>
          </div>
          <div className="col-1 py-2 linkHeader">
            <Link to="/cocktail">Cocktails</Link>
          </div>
        </div>
        </header>
        <main className="py-5">
          <Switch>
            <Route exact path="/">
              <Homepage />
            </Route>
            <Route path="/cocktail">
              <CocktailList />
            </Route>
          </Switch>
        </main>
        <footer className="container-fuild text-center pt-3 fixed-bottom">
          <FontAwesomeIcon icon={faCopyright} /> GerenaDrunk edited by "Les 2 mecs du fond"
        </footer>
      </Router>
    </div>
  );
}

export default App;
