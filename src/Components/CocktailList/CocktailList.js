import React, { useState, useEffect } from 'react';
import Cocktail from '../Cocktail/Cocktail';
import Card from '../Card/Card';
import load from '../../Asset/images/load2.gif';
import {
  Switch,
  Route,
  useRouteMatch
} from "react-router-dom";

const CocktailList = () => {

  const [drinks, setDrinks] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [errors, setErrors] = useState(null);
  const [ingredient, setIngredient] = useState('');

  // Component did mount -> get all cocktails

  useEffect(() => {
    fetch("https://the-cocktail-db.p.rapidapi.com/filter.php?c=Cocktail", {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
        "x-rapidapi-key": "7b9200bb94msheede853aada6416p1bf623jsn1fd6ea76330d",
        "Access-Control-Allow-Origin": "crossorigin"
      },
    })
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setDrinks(result.drinks);
        },
        (error) => {
          console.log(error);
          setIsLoaded(true);
          setErrors(error);
        }
      );
    return () => {
      console.log("componentWillUnmount");
    };
  }, []);

  const handleFilter = (event) => {
    fetch("https://the-cocktail-db.p.rapidapi.com/filter.php?i="+ingredient, {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
        "x-rapidapi-key": "14926dc01cmsh9101029696f10d6p1aaa87jsn4f66218e4573",
        "Access-Control-Allow-Origin": "crossorigin"
      }
    })
    .then(res => res.json())
    .then(result => {
        if(result.drinks){
          setIsLoaded(true);
          setIngredient(ingredient);
          setDrinks(result.drinks);
        }else{
          setIsLoaded(true);
          setIngredient(ingredient);
        }
        
    })
    .catch(error => {
      console.log(error);
      setIsLoaded(true);
      setErrors(error);
    });
    event.preventDefault();
  }
  const handleIngredient = (event) => {
    setIngredient(document.getElementById('ingredient').value);
    event.preventDefault();
  };

  let { path } = useRouteMatch();

  if (errors) {
    return <div>Erreur : {errors.message}</div>;
  } else if (!isLoaded) {
    return <div className="Cocktail container" >
    <div className="row cocktail">
      <div className="col-12 text-center">
        <img src={load} alt="logo" className="logo"/>
      </div>
    </div>
  </div>;
  } else {
    return (
      <Switch>
        <Route exact path={path}>
          <div className="CocktailList container" >
            <div className="row">
              <div className="col-12 text-center">
                <h2>Les Cocktails</h2>
              </div>
              <div className="col-12 text-center py-2">
                <label>Filter by</label>
                <input className="mx-3 px-2" type="text" name="ingredient" id="ingredient" value={ingredient} onChange={handleIngredient}></input>
                <button type="submit" onClick={handleFilter}>Filter</button>
              </div>
              <div className="col-12 text-center">
                  <div className="row justify-content-center">
                    {
                        
                    drinks.map((drink, index) => {
                      return (<Card {...drink} key={index} />)
                    })} 
                  </div>
              </div>
            </div>
          </div>
        </Route>
        <Route path={`${path}/:cocktailId`}>
          <Cocktail />
        </Route>
      </Switch>

    );

  }

}

export default CocktailList;