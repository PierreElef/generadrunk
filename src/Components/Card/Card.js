import React from 'react';
import {
    Link,
    useRouteMatch
} from "react-router-dom";

const Card = drink => {
    let { path } = useRouteMatch();
    return (
        <Link to={path + '/' + drink.idDrink} className="card col-3 ml-5 mb-5 pt-3 card">
            <img src={drink.strDrinkThumb} height='180' alt="boisson"></img>
            <div className="card-body">
                <h5 className="card-title">{drink.strDrink}</h5>
            </div>
        </Link> 
    );
}

export default Card;