import React from 'react';
import load from '../../Asset/images/load.gif';
class Homepage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      drinks: []
    };
  }

  componentDidMount() {
    fetch("https://the-cocktail-db.p.rapidapi.com/random.php", {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
        "x-rapidapi-key": "14926dc01cmsh9101029696f10d6p1aaa87jsn4f66218e4573",
        "Access-Control-Allow-Origin": "crossorigin"
      }
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
          this.setState({
            isLoaded: true,
            drinks: result.drinks
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, drinks } = this.state;
    if (error) {
      return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
      return <div className="Cocktail container" >
        <div className="row cocktail">
          <div className="col-12 text-center">
            <img src={load} alt="logo" className="logo"/>
          </div>
        </div>
      </div>;
    } else {
      return (
        <div className="Cocktail container" >
          <div className="row">
            <div className="col-12 text-center">
              <h5>There's a drink for every mood or occasion. Find your favorite cocktail recipes below. Or think outside the glass and try a brand new drink. Your new favorite could be a shake or stir away.</h5>
            </div>
          </div>
        <div className="row cocktail my-5">
          <div className="col-12 text-center">
            <h2>{drinks[0].strDrink}</h2>
          </div>
          <div className="col-4 mb-5">
            <div className="img">
              <img src={drinks[0].strDrinkThumb} alt="boisson"></img>
            </div>
          </div>
          <div className="ingredients offset-1 col-7">
            <div className='row mt-3'>
              <div className="col-6">
                <h3>Ingredients</h3>
              </div>
              <div className="col-6">
                <h3>Quantity</h3>
              </div>
              <div className="col-6">
                {drinks[0].strIngredient1}
              </div>
              <div className="col-6">
                {drinks[0].strMeasure1}
              </div>
              <div className="col-6">
                {drinks[0].strIngredient2}
              </div>
              <div className="col-6">
                {drinks[0].strMeasure2}
              </div>
              <div className="col-6">
                {drinks[0].strIngredient3}
              </div>
              <div className="col-6">
                {drinks[0].strMeasure3}
              </div>
              <div className="col-6">
                {drinks[0].strIngredient4}
              </div>
              <div className="col-6">
                {drinks[0].strMeasure4}
              </div>
            </div>
            <div className="row">
              <h3 className="col-12 mt-3">Instructions</h3>
              <div className="col-12">
                {drinks[0].strInstructions}
              </div>
            </div>
          </div>
        </div>
      </div>
      );
    }
  }
}

export default Homepage;