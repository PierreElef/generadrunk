import React, { useState, useEffect } from 'react';
import {
  useParams,
} from "react-router-dom";

const Cocktail = () => {

  const [drink, setDrink] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [errors, setErrors] = useState(null);

  let { cocktailId } = useParams();

  useEffect(() => {

    fetch("https://the-cocktail-db.p.rapidapi.com/lookup.php?i=" + cocktailId, {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
        "x-rapidapi-key": "7b9200bb94msheede853aada6416p1bf623jsn1fd6ea76330d",
        "Access-Control-Allow-Origin": "crossorigin"
      },
    })
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setDrink(result.drinks[0]);
        },
        (error) => {
          console.log(error);
          setIsLoaded(true);
          setErrors(error);
        }
      );
    return () => {
      console.log("componentWillUnmount");
    };
  }, []);

  return (
    <div className="Cocktail container" >
      <div className="row cocktail">
        <div className="col-12 text-center">
          <h2>{drink.strDrink}</h2>
        </div>
        <div className="col-4 mb-5">
          <div className="img">
            <img src={drink.strDrinkThumb} alt="boisson"></img>
          </div>
        </div>
        <div className="ingredients offset-1 col-7">
          <div className='row mt-3'>
            <div className="col-6">
              <h3>Ingredients</h3>
            </div>
            <div className="col-6">
              <h3>Quantity</h3>
            </div>
            <div className="col-6">
              {drink.strIngredient1}
            </div>
            <div className="col-6">
              {drink.strMeasure1}
            </div>
            <div className="col-6">
              {drink.strIngredient2}
            </div>
            <div className="col-6">
              {drink.strMeasure2}
            </div>
            <div className="col-6">
              {drink.strIngredient3}
            </div>
            <div className="col-6">
              {drink.strMeasure3}
            </div>
            <div className="col-6">
              {drink.strIngredient4}
            </div>
            <div className="col-6">
              {drink.strMeasure4}
            </div>
          </div>
          <div className="row">
            <h3 className="col-12 mt-3">Instructions</h3>
            <div className="col-12">
              {drink.strInstructions}
            </div>
          </div>
        </div>
      </div>
    </div>
  );

}

export default Cocktail;